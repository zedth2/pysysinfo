#!/usr/bin/python3
'''
Author : Zachary Harvey

Available output columns: Taken from lsblk for reference
        NAME  device name
       KNAME  internal kernel device name
        PATH  path to the device node
     MAJ:MIN  major:minor device number
     FSAVAIL  filesystem size available
      FSSIZE  filesystem size
      FSTYPE  filesystem type
      FSUSED  filesystem size used
      FSUSE%  filesystem use percentage
  MOUNTPOINT  where the device is mounted
       LABEL  filesystem LABEL
        UUID  filesystem UUID
      PTUUID  partition table identifier (usually UUID)
      PTTYPE  partition table type
    PARTTYPE  partition type UUID
   PARTLABEL  partition LABEL
    PARTUUID  partition UUID
   PARTFLAGS  partition flags
          RA  read-ahead of the device
          RO  read-only device
          RM  removable device
     HOTPLUG  removable or hotplug device (usb, pcmcia, ...)
       MODEL  device identifier
      SERIAL  disk serial number
        SIZE  size of the device
       STATE  state of the device
       OWNER  user name
       GROUP  group name
        MODE  device node permissions
   ALIGNMENT  alignment offset
      MIN-IO  minimum I/O size
      OPT-IO  optimal I/O size
     PHY-SEC  physical sector size
     LOG-SEC  logical sector size
        ROTA  rotational device
       SCHED  I/O scheduler name
     RQ-SIZE  request queue size
        TYPE  device type
    DISC-ALN  discard alignment offset
   DISC-GRAN  discard granularity
    DISC-MAX  discard max bytes
   DISC-ZERO  discard zeroes data
       WSAME  write same max bytes
         WWN  unique storage identifier
        RAND  adds randomness
      PKNAME  internal parent kernel device name
        HCTL  Host:Channel:Target:Lun for SCSI
        TRAN  device transport type
  SUBSYSTEMS  de-duplicated chain of subsystems
         REV  device revision
      VENDOR  device vendor
       ZONED  zone model

'''

from pysysfs import disks
from pysysfs import consts
from pprint import pprint
import argparse
from sys import argv

TEE = '├'
ELBOW = '└'
VPIPE = '|'
PIPE = '─'
default_columns = ['NAME', 'MAJ:MIN', 'TYPE', 'SIZE', 'MOUNTPOINT']

def str_size(size):
    tup = consts.find_smallest_I(size)
    s = str(round(tup[0], 1))
    return s + tup[1][0]

column_func = {
            'NAME': lambda d: d.devname,
            'MAJ:MIN': lambda d: str(d.major) + ':' + str(d.minor),
            'TYPE': lambda d: d.devtype,
            'SIZE': lambda d: str_size(d.totalsize),
            'MOUNTPOINT': lambda d: d.mount,
            }



def column_lengths(stuff):
    columns = [0] * len(stuff[0])
    reVals = ''
    for l in stuff:
        cnt = 0
        while cnt < len(l):
            if columns[cnt] < len(l[cnt]):
                columns[cnt] = len(l[cnt])
            cnt += 1
    for c in columns:
        reVals += '{:' + str(c+2) + '}'
    return reVals

def print_tree(all_disks, columns):
    keys = list(all_disks.keys())
    keys.sort()
    out = ''
    lines = []
    longest = -1
    for k in keys:
        for i in all_disks[k]:
            if isinstance(i, list):
                lines.append(i)
                if longest < len(i[0]):
                    longest = len(i[0])
            elif isinstance(i, dict):
                # clines =
                sort_keys = list(i.keys())
                sort_keys.sort()
                childs, nlong = print_tree(i, columns)
                if nlong > longest: longest = nlong
                cnt = 0
                char = TEE
                last = False
                while cnt < len(childs):
                    if cnt+1 == len(childs) or childs[cnt][0] == sort_keys[-1]:
                        char = ELBOW
                        last = True

                    if childs[cnt][0][0].isalpha():
                        l = childs[cnt]
                        l[0] = char + PIPE + l[0]
                        lines.append(l)
                    elif childs[cnt][0].startswith((' ', TEE, ELBOW, PIPE, VPIPE)):
                        l = childs[cnt]
                        if last:
                            l[0] = ' ' + childs[cnt][0]
                        else:
                            l[0] = VPIPE + ' ' + childs[cnt][0]
                        lines.append(l)
                    else:
                        lines.append(childs[cnt])
                    if longest < len(lines[-1][0]):
                        longest = len(lines[-1][0])
                    cnt += 1
                # lines.append(' ' + first + childs[0])
                # if len(childs) > 1:
                    # for c in childs[1:-1]:
                        # lines.append(' ' + TEE + c)
                    # lines.append(' ' + ELBOW + childs[-1])
                # lines.append

    return lines, longest


def load_disk_columns(columns, dsk):
    children = {}
    for p in dsk.parts:
        children.update(load_disk_columns(columns, p))
    for h in dsk.holders:
        children.update(load_disk_columns(columns, h))
    return {dsk.devname: [load_columns(columns, dsk), children]}

def load_columns(columns, dsk):
    reVals = []
    for c in columns:
        item = column_func[c](dsk)
        if item == None: item = ''
        reVals.append(item)
    return reVals

def main(args):
    wanted = []
    if 0 == len(args.disks):
        wanted = disks.find_all_disks()
    else:
        wanted = disks.find_disks(*args.disks)
    if args.json:
        for d in wanted:
            pprint(d.to_dict())
        return

    out_disks = {}
    for d in wanted:
        out_disks.update(load_disk_columns(default_columns, d))

    lines, longest = print_tree(out_disks, default_columns)

    lines.insert(0, default_columns)
    form = column_lengths(lines)
    for l in lines:
        print(form.format(*l))

def arg_setup(*args):
    parser = argparse.ArgumentParser(description='Load disk info')
    parser.add_argument('--json', '-J', dest='json', action='store_true', default=False, help='Print all info in JSON format')
    parser.add_argument('--disks', '-d', dest='disks', default=[], nargs='*', help='Print info only on these disks')
    return parser.parse_args(args=args[1:])

if __name__ == '__main__':
    try:
        main(arg_setup(*argv))
    except disks.NoDisk as e:
        print('ERROR : ', *e.args)
