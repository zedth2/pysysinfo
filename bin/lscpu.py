#!/usr/bin/python3

from pysysfs import cpuspp
from pprint import pprint
from sys import argv
import argparse

def main():
    a = argsetup(*argv)
    # print(a)
    print_func = print
    switch = False
    if a.pretty:
        print_func = pprint
    if a.cache:
        cac = list(cpuspp.Cache.load_sys_caches())
        cac.sort(key=lambda c: int(c.id))
        for l in cac:
            print_func(l.to_dict())
        switch = True
    if a.core is not None:
        for c in a.core:
            cor = cpuspp.CPUCore.loadsyspath(c)
            print_func(cor.to_dict())
        switch = True
    if a.usage:
        cpus = cpuspp.CPUInfo.load_all_sys_cpu()
        for cpu in cpus:
            core_ids = list(cpu.cores.keys())
            core_ids.sort()
            for id in core_ids:
                print_func(id, ' : ', cpu.cores[id].get_percentage())
        switch = True
    if not switch:
        for c in cpuspp.CPUInfo.load_all_sys_cpu():
            print_func(c.to_dict())

def argsetup(*args):
    parser = argparse.ArgumentParser(prog='pycpu', usage='%(prog)s [options]')
    parser.add_argument('--cache', '-c', action='store_true', help='Get info only on the caches')
    parser.add_argument('--core', '-o', nargs='+', help='Get info only on specified cores')
    parser.add_argument('--pretty', '-p', action='store_true', help='Get info only on specified cores')
    parser.add_argument('--usage', '-u', action='store_true', help='Print core usages')

    return parser.parse_args(args[1:])


if __name__ == '__main__':
    main()
