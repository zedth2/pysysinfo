#!/usr/bin/python3
'''
Author : Zachary Harvey






'''

from subprocess import Popen, PIPE, TimeoutExpired
from shutil import which
from os.path import exists
import re
SFDISK = which('sfdisk')

def get_output(*args):
    if not exists(which(SFDISK)):
        raise Exception(SFDISK + ' not on PATH')
    pop = Popen([SFDISK] + list(args), stdout=PIPE, stderr=PIPE)
    # try:
    stdout, stderr = pop.communicate(timeout=1)
    # except TimeoutExpired:
    return stdout.decode()

def _gettypes():
    reVal = {}
    rehex = re.compile('[a-f0-9]+')
    for l in get_output('-T').splitlines():
        m = rehex.match(l)
        if m is None:
            continue
        k, v = l.split(maxsplit=1)
        k = k.strip()
        v = v.strip()
        reVal[k] = v
    return reVal
fdisk_types = _gettypes()
def get_type(type):
    return fdisk_types[type]

def raw_dump(devpath):
    return get_output('-d', devpath)

def parse_dump(dump):
    reVal = {}
    for line in dump.splitlines():
        try:
            k, v = line.split(':', 1)
            reVal[k.strip()] = v.strip()
        except ValueError:
            pass
        if line.startswith('/dev/'):
            items = {}
            for i in reVal[k].split(','):
                k, v = i.split('=')
                k = k.strip()
                v = v.strip()
                if k == 'type' and k in fdisk_types:
                    v = fdisk_types[k]
                items[k] = v
            reVal[k] = items
        else:
            pass
    return reVal

def dump(devpath):
    return parse_dump(raw_dump(devpath))



if __name__ == '__main__':
    from sys import argv
    from pprint import pprint
    x = dump(argv[1])
    pprint(x)
