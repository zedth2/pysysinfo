#!/usr/bin/python3
'''
Author : Zachary Harvey






'''
from sys import argv, exit
import argparse
from json import dump, dumps
from sys import stdout

# from pprint import pprint

from pysysfs import mem
from pysysfs.consts import find_smallest_B, find_smallest_I
from pysysfs.consts import SIZE_BYTE
def main(*args):

    arg = arg_parse(*args)
    if arg.json:
        if arg.pretty:
            dump(mem.full_summary(), stdout, indent=2)
        else:
            dump(mem.full_summary(), stdout)
        print('\n')
        return 0

    m = mem.Memory.load_from_meminfo()
    total = str(m.total) + SIZE_BYTE
    used_mem = str(m.used_mem) + SIZE_BYTE
    ava = str(m.mem_available) + SIZE_BYTE
    shared = str(m.shared) + SIZE_BYTE
    buffers = str(m.buffers) + SIZE_BYTE
    cached = str(m.cached) + SIZE_BYTE
    if arg.human:
        func = find_smallest_I
        if arg.si:
            func = find_smallest_B
        total = find_small(m.total, func, 1)
        used_mem = find_small(m.used_mem, func, 1)
        ava = find_small(m.mem_available, func, 1)
        shared = find_small(m.shared, func, 1)
        buffers = find_small(m.buffers, func, 1)
        cached = find_small(m.cached, func, 1)
    form = "{:<7} {:>10} {:>10} {:>10} {:>10} {:>10} {:>10}"
    header = form.format("", "total", "used", "free", "shared", "buffers", "cached", "available")
    memstr = form.format("Mem:", total, used_mem, ava, shared, buffers, cached, "")
    print(header, '\n', memstr)

def find_small(size, func, round=2):
    b, s = func(size)
    f = "{:."+str(round)+"f}{}"
    return f.format(b, s)

def arg_parse(*args):
    par = argparse.ArgumentParser(prog=args[0], description='Display memory information', add_help=False)
    par.add_argument('--human', '-h', help='Print sizes in human readable shortest sizes', action='store_true')
    par.add_argument('--si', help='Use kilo, mega, giga etc (power of 1000) instead of kibi, mebi, gibi (power of 1024)', action='store_true')
    par.add_argument('--help', help='Print this help messages', action='help')
    par.add_argument('--json', '-j', help='Print raw JSON data', action='store_true', default=False)
    par.add_argument('--pretty', '-p', help='Print JSON pretty', action='store_true', default=False)
    return par.parse_args(args[1:])

if __name__ == '__main__':
    exit(main(*argv))
