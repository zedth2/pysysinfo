#!/usr/bin/python3
'''
Author : Zachary Harvey






'''


import time
import pprint
import glob
from os.path import basename, isdir
import re
def getline():
    return open('/proc/stat').readlines()[1]

def main():
    last_active_total = 0
    last_idle_total = 0
    cnt = 0
    while cnt < 100:
        line = getline().split()
        user = int(line[1])
        nice = int(line[2])
        system = int(line[3])
        idle = int(line[4])
        iowait = int(line[5])
        irq = int(line[6])
        softirq = int(line[7])
        steal = int(line[8])
        active_total = user + nice + system + irq + softirq + steal
        cur_idle = idle + iowait
        # cur_total = total - last_active_total
        val = 1.0
        if (active_total - last_active_total) != 0.0:
            val = abs((active_total - last_active_total) - (cur_idle - last_idle_total)) / abs((active_total+cur_idle) - (last_active_total+last_idle_total))

        print(val)
        last_active_total = active_total
        last_idle_total = cur_idle
        cnt += 1
        time.sleep(4)

class NoCaseDict(dict):
    def __setitem__(self, key, val):
        return super().__setitem__(key.lower(), val)
    def __getitem__(self, key):
        return super().__getitem__(key.lower())

class UnknownCore(ValueError):
    def __init__(self, corepath, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.corepath = corepath

CPU_INFO = '/proc/cpuinfo'
STAT = '/proc/stat'

SYS_CPU_PATH = '/sys/devices/system/cpu'
SYS_CPU_CORE_PATH = '/sys/devices/system/cpu/{}'
SYS_CPU_TOPOLOGY = SYS_CPU_PATH + '/{}/topology/'
SYS_CPU_CACHE = SYS_CPU_PATH + '/{}/cache'
SYS_CPU_CACHE_INDEX_GLOB = SYS_CPU_CACHE + '/index*'
SYS_CPU_CORE_ID = SYS_CPU_TOPOLOGY + '/core_id'
SYS_CPU_CORE_SIB = SYS_CPU_TOPOLOGY + '/core_siblings'
SYS_CPU_CORE_SIB_LIST = SYS_CPU_TOPOLOGY + '/core_siblings_list'
SYS_CPU_THREAD_SIB = SYS_CPU_TOPOLOGY + '/thread_siblings'
SYS_CPU_THREAD_SIB_LIST = SYS_CPU_TOPOLOGY + '/thread_siblings_list'
SYS_CPU_PHYS_ID = SYS_CPU_TOPOLOGY + '/physical_package_id'

### CPU_KEYS
### These were taken from lscpu source code
PROCESSOR = "processor"
VENDOR_ID = ("vendor_id", "CPU implementer", "vendor")
CPU_FAM = ("family", "cpu family")
MODEL = ("model", "CPU part")
MODEL_NAME = "model name"
STEPPING = ("CPU variant", "stepping")
MICROCODE = "microcode"
REVISION = ("revision", "CPU revision")
CPU = "cpu"
CPU_MHZ = "cpu MHz"
CPU_MHZ_DYN = "cpu MHz dynamic"
CPU_MHZ_STATIC = "cpu MHz static"
CACHE_SIZE = "cache size"
PHYSICAL_ID = "physical id"
SIBLINGS = "siblings"
CORE_ID = "core id"
CPU_CORES = "cpu cores"
APICID = "apicid"
INITIAL_APICID = "initial apicid"
FPU = "fpu"
FPU_EXCEPTION = "fpu_exception"
CPUID_LEVEL = "cpuid level"
WP = "wp"
FLAGS = ("flags", "features", "Features", "type")
BUGS = "bugs"
BOGOMIPS = ("bogomips", "BogoMIPS", "bogomips per cpu")
TLB_SIZE = "TLB size"
CLFLUSH_SIZE = "clflush size"
CACHE_ALIGNMENT = "cache_alignment"
ADDRESS_SIZES = "address sizes"
POWER_MANAGEMENT = "power management"
MAX_THREAD_ID = "max thread id"
ADD_SIZE = "address sizes"


class CPUInfo:
    def __init__(self, physicalid=0):
        self.extras = {}
        self.cores = {}
        self.model_name = ''
        self.physical_id = physicalid
        self.core_id = None

    @property
    def corecount(self):
        return len(self.cores)

    def percentages(self):
        perc = {}
        for k, v in self.cores.items():
            perc[k] = v.get_percentage()
        return perc

    def process_info(self, info):
        if isinstance(info, dict):
            return self.__process_info(info)
        elif not isinstance(info, (list, tuple)):
            raise TypeError('')
        for i in info:
            self.__process_info(i)

    def __process_info(self, info):
        if not isinstance(info, dict):
            raise TypeError('')
        if self.physical_id != info[PHYSICAL_ID]:
            return False
        self.cores[info[CORE_ID]] = CPUCore.process_info(info)
        self.model_name = info[MODEL_NAME]
        return True

    def addcore(self, sysbase):
        core = CPUCore.loadsyspath(sysbase)
        self.cores[core.core_id] = core

    @classmethod
    def from_dict(cls, dct):
        cpu = cls(dct['physical_id'])
        cpu.model_name = dct['model_name']
        for c in dct['cores']:
            cpu.cores[c['core_id']] = CPUCore.from_dict(c)
        return cpu

    def to_dict(self):
        return  {
                'model_name' : self.model_name,
                'physical_id': self.physical_id,
                'cores' : [ c.to_dict() for k, c in self.cores.items() ]
                }

    @staticmethod
    def parse_cpu_info(cpuinfo=CPU_INFO):
        block = []
        cur = {}
        for l in open(cpuinfo).readlines():
            if l.strip() == '':
                block.append(cur)
                cur = {}
                # print(block[-1].keys())
            else:
                cur[l.split(':', 1)[0].strip()] = l.split(':', 1)[1].strip()
        return block

    @classmethod
    def get_all_proc_cpus(cls, cpuinfo=CPU_INFO):
        return cls.process_all_info(cls.parse_cpu_info(cpuinfo))

    @classmethod
    def process_all_info(cls, infos):
        cpus = {}
        for c in infos:
            if c[PHYSICAL_ID] not in cpus:
                cpus[c[PHYSICAL_ID]] = cls(c[PHYSICAL_ID])
            cpus[c[PHYSICAL_ID]].process_info(c)
        return tuple(cpus.values())

    @classmethod
    def load_all_sys_cpu(cls):
        allpaths = [basename(p) for p in glob.glob(SYS_CPU_PATH + '/cpu[0-9]*')]
        physids = {}
        for p in allpaths:
            physid = open(SYS_CPU_PHYS_ID.format(p)).readlines()[0].strip()
            if not physid in physids:
                physids[physid] = cls(physid)
            physids[physid].addcore(p)
        return tuple(physids.values())

class CPUCore:
    def __init__(self, coreid, parentphysicalid=0):
        self.core_id = coreid
        self.parent = parentphysicalid
        self.mhz = 0
        self.cache_size = 0
        self.caches = tuple()
        self.__last_active_total = 0
        self.__last_idle_total = 0
        self._load_caches()

    def get_percentage(self):
        '''
        Code was been taken from
        https://rosettacode.org/wiki/Linux_CPU_utilization
        and rewriten to support current design
        '''
        vals = CPUCore.get_stat_line(self.core_id)
        total = sum(vals.values())
        # pprint.pprint(vals)
        idle = vals['idle']
        idleDelta = idle - self.__last_idle_total
        # print(idleDelta)
        totalDelta = total - self.__last_active_total
        # print(totalDelta)
        percent = 100.0 * (1.0 - (idleDelta / totalDelta))
        self.__last_idle_total = idle
        self.__last_active_total = total
        return percent

    def _load_caches(self):
        indexs = glob.glob(SYS_CPU_CACHE_INDEX_GLOB.format('cpu'+str(self.core_id)))
        caches = []
        for i in indexs:
            caches.append(Cache.load_sys_cache(i))
        caches.sort(key=lambda x: int(x.id))
        self.caches = tuple(caches)

    @classmethod
    def process_info(cls, info):
        core = cls(info[CORE_ID], info[PHYSICAL_ID])
        core.mhz = info[CPU_MHZ]
        core.cache_size = info[CACHE_SIZE]
        return core

    @staticmethod
    def get_stat_line(core_id):
        line = ''
        vals = {}
        with open(STAT) as s:
            for l in s.readlines():
                if l.startswith('cpu'+str(core_id)):
                    line = l
                    break
        split = line.split()
        vals['user'] = int(split[1])
        vals['nice'] = int(split[2])
        vals['system'] = int(split[3])
        vals['idle'] = int(split[4])
        vals['iowait'] = int(split[5])
        vals['irq'] = int(split[6])
        vals['softirq'] = int(split[7])
        vals['steal'] = int(split[8])
        vals['guest'] = int(split[9])
        vals['guest_nice'] = int(split[10])
        return vals

    @classmethod
    def from_dict(cls, dct):
        core = cls(dct['core_id'], dct['parent_id'])
        core.mhz = dct['mhz']
        core.cache_size = dct['cache_size']
        caches = []
        for c in dct['caches']:
            caches.append(Cache.from_dict(c))
        return core

    def to_dict(self):
        return  {
                'core_id':self.core_id,
                'parent_id':self.parent,
                'mhz': self.mhz,
                'cache_size':self.cache_size,
                'caches': tuple([c.to_dict() for c in self.caches])
                }

    def __str__(self):
        pass

    @classmethod
    def loadsyspath(cls, cpu='cpu0'):
        if isinstance(cpu, int):
            cpu = 'cpu' + str(cpu)
        elif isinstance(cpu, str) and cpu.isdigit():
            cpu = 'cpu' + cpu
        cpu_path = SYS_CPU_CORE_PATH.format(cpu)
        if not isdir(cpu_path):
            raise UnknownCore(cpu_path, '{} does not exist'.format(cpu_path))
        phys_id = open(SYS_CPU_PHYS_ID.format(cpu)).read().strip()
        core_id = open(SYS_CPU_CORE_ID.format(cpu)).read().strip()
        corepu = cls(core_id, phys_id)
        return corepu

    @staticmethod
    def get_single_percentage(coreid, physical_id=0, samples=5, sleeptime=.1):
        cnt = 0
        perc = 0.0
        c = CPUCore.loadsyspath()

class Cache:
    def __init__(self, id, level, size):
        self.id = id
        self.level = level
        self.size = size
        self.__cpu_parents = tuple()

    def __set_cpu_parents(self, cpus):
        if isinstance(cpus, list):
            self.__cpu_parents = tuple(cpus)
        elif isinstance(cpus, str):
            if cpus.isdigit():
                self.__cpu_parents = (int(cpus),)
                return
            digone = None
            digtwo = None
            for c in cpus:
                if c.isdigit():
                    if digone is None:
                        digone = int(c)
                    elif digtwo is None:
                        digtwo = int(c)
            self.__cpu_parents = tuple(range(digone, digtwo+1))
        elif isinstance(cpus, int):
            self.__cpu_parents = (cpus,)
        elif isinstance(cpus, tuple):
            self.__cpu_parents = cpus
        else:
            raise TypeError('Type provided is not supported')

    def __get_cpu_parents(self):
        return self.__cpu_parents
    cpu_parents = property(__get_cpu_parents, __set_cpu_parents)

    def to_dict(self):
        return  {
                'id': self.id,
                'level': self.level,
                'size': self.size,
                'parents':self.cpu_parents,
                }

    @classmethod
    def from_dict(cls, dct):
        cache = Cache(dct['id'], dct['level'], dct['size'])
        cache.cpu_parents = dct['parents']
        return cache

    @staticmethod
    def load_sys_caches(glb=SYS_CPU_PATH+'/cpu*/cache/index*'):
        caches = {}
        for c in glob.glob(glb):
            cac = Cache.load_sys_cache(c)
            if cac.id in caches:
                continue
            caches[cac.id] = cac
        return tuple(caches.values())

    @classmethod
    def load_sys_cache(cls, full_cache_path):
        id = open(full_cache_path+'/id').readline().strip()
        level = open(full_cache_path+'/level').readline().strip()
        size = open(full_cache_path+'/size').readline().strip()
        cache = cls(id, level, size)
        cache.cpu_parents = open(full_cache_path+'/shared_cpu_list').readline().strip()

        return cache
