#!/usr/bin/python3


SIZE_BYTE = 'B'
SIZE_SECTOR = 's'
SIZE_KiB = 'KiB'
SIZE_KB = 'kB'
SIZE_MiB = 'MiB'
SIZE_MB = 'MB'
SIZE_GiB = 'GiB'
SIZE_GB = 'GB'
SIZE_TiB = 'TiB'
SIZE_TB = 'TB'
SIZE_YiB = 'YiB'
SIZE_YB = 'YB'
SIZE_ZiB = 'ZiB'
SIZE_ZB = 'ZB'
SIZES = [SIZE_SECTOR, SIZE_BYTE, 'kB', 'MB', 'MiB', 'GB', 'GiB', 'TB', 'TiB']
SIZE_BYTE,

B_SIZES = [
        SIZE_KB,
        SIZE_MB,
        SIZE_GB,
        SIZE_TB,
        "PB",
        "EB",
        SIZE_ZB,
        SIZE_YB
          ]

I_SIZES = [
        SIZE_KiB,
        SIZE_MiB,
        SIZE_GiB,
        SIZE_TiB,
        "PiB",
        "EiB",
        SIZE_ZiB,
        SIZE_YiB
          ]

SIZE_EXPONENTS = {
    SIZE_BYTE:  1,       # byte
    SIZE_KB:    1000**1, # kilobyte
    SIZE_MB:    1000**2, # megabyte
    SIZE_GB:    1000**3, # gigabyte
    SIZE_TB:    1000**4, # terabyte
    "PB":       1000**5, # petabyte
    "EB":       1000**6, # exabyte
    SIZE_ZB:    1000**7, # zettabyte
    SIZE_YB:    1000**8, # yottabyte

    SIZE_KiB:  1024**1, # kibibyte
    SIZE_MiB:  1024**2, # mebibyte
    SIZE_GiB:  1024**3, # gibibyte
    SIZE_TiB:  1024**4, # tebibyte
    "PiB":     1024**5, # pebibyte
    "EiB":     1024**6, # exbibyte
    SIZE_ZiB:  1024**7, # zebibyte
    SIZE_YiB:  1024**8  # yobibyte
}

class UnknownSize(ValueError):
    pass

def toBytes(size, units):
    return float(size) * SIZE_EXPONENTS[units]

def parseSize(size):
    if not isinstance(size, str):
        raise ValueError('Expecting a string for variable size')
    num = ''
    unit = ''
    cnt = 0
    while cnt < len(size):
        if size[cnt].isdigit() or size[cnt] == '-':
            num += size[cnt]
        else:
            break
        cnt += 1
    return float(num), size[cnt:].strip()

def toSize(size, inunit, outunit):
    if not (inunit in SIZE_EXPONENTS and outunit in SIZE_EXPONENTS):
        raise ValueError('inunit and outunit must be in SIZE_EXPONENTS')
    return toBytes(size, inunit) / SIZE_EXPONENTS[outunit]

def find_smallest_B(size, inunit=SIZE_BYTE):
    return __find_smallest(size, inunit, B_SIZES)

def find_smallest_I(size, inunit=SIZE_BYTE):
    return __find_smallest(size, inunit, I_SIZES)

def __find_smallest(size, inunit, sizes):
    cnt = 1
    bytesize = toSize(size, inunit, SIZE_BYTE)
    outsize = SIZE_BYTE
    while cnt < len(sizes):
        if SIZE_EXPONENTS[sizes[cnt-1]] < bytesize and SIZE_EXPONENTS[sizes[cnt]] > bytesize:
            outsize = sizes[cnt-1]
            break
        cnt += 1

    return toSize(bytesize, SIZE_BYTE, outsize), outsize

def get_first_line(fullpath):
    f = open(fullpath)
    l = f.readlines()[0].strip()
    f.close()
    return l

if __name__ == '__main__':
    print(toSize(937699328*512, SIZE_BYTE, 'GiB'))
