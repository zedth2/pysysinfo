#!/usr/bin/python3
'''
Author : Zachary Harvey






'''

from .consts import parseSize, toSize, SIZE_BYTE, SIZE_GiB, SIZE_GB

import pprint

MEMINFO = '/proc/meminfo'
PROC_SWAPS = '/proc/swaps'

def read_meminfo(meminfo=MEMINFO):
    '''
    Reads all values from meminfo file.

    meminfo : str of path
        A path to a file in the same format as /proc/meminfo

    return : dict
        Keys are the left side of the colon, :, as a string.
        Values are the right side of the colon. A string if the
        size fails to parse otherwise a float in size bytes.
    '''
    vals = {}
    with open(meminfo) as m:
        for l in m.readlines():
            k, v = l.split(':')
            try:
                vals[k.strip()] = toSize(*parseSize(v.strip()), SIZE_BYTE)
            except ValueError:
                vals[k.strip()] = v.strip()
    return vals

def read_swaps(swaps=PROC_SWAPS):
    actswaps = []
    with open(swaps) as s:
        for l in s.readlines()[1:]:
            actswaps.append(Swap.load_from_swaps_line(l))
    return tuple(actswaps)


MEMFREE = 'MemFree'
MEMAVAILABLE = 'MemAvailable'
MEMTOTAL = 'MemTotal'
MEMSHARED = 'Shmem'

SWAPTOTAL = 'SwapTotal'
SWAPFREE = 'SwapFree'
BUFFERS = 'Buffers'
CACHED = 'Cached'
SRECLAIMABILE = 'SReclaimable'

class Swap:
    def __init__(self, total, type, filename, priority):
        self.__total = total
        self.__type = type
        self.__filename = filename
        self.__priority = priority

    @property
    def total(self):
        return self.__total

    @property
    def type(self):
        return self.__type

    @property
    def filename(self):
        return self.__filename

    @property
    def priority(self):
        return self.__priority

    def current_use(self, swaps=PROC_SWAPS):
        use = -1
        with open(swaps) as s:
            lines = s.readlines()[1:]
            for l in lines:
                if l.startswith(self.filename):
                    use = float(l.split()[3].strip())
                    break
        return use

    def to_dict(self):
        return {
                'total': self.total,
                'type': self.type,
                'filename': self.filename,
                'priority': self.priority,
                'used': self.current_use(),
                }

    @classmethod
    def from_dict(cls, dct):
        return cls(dct['total'], dct['type'], dct['filename'], dct['priority'])

    @classmethod
    def load_from_swaps_line(cls, line):
        split = line.split()
        return cls(split[2].strip(), split[1].strip(), split[0].strip(), split[4].strip())

class Memory:
    def __init__(self, total, swap, meminfo=MEMINFO, swaps=PROC_SWAPS):
        self.total = total
        self.swap = swap
        self.meminfo = meminfo
        self.swapinfo = swaps

    @property
    def used_mem(self):
        f = self.__find_line(MEMFREE)
        t = self.total
        m = t - f - self.cached - self.shared
        if m < 0:
            return t - f
        return m

    @property
    def used_swap(self):
        return self.swap - self.__find_line(SWAPFREE)

    @property
    def mem_available(self):
        return self.__find_line(MEMAVAILABLE)

    @property
    def shared(self):
        return self.__find_line(MEMSHARED)

    @property
    def buffers(self):
        return self.__find_line(BUFFERS)

    @property
    def cached(self):
        return self.__find_line(CACHED) + self.__find_line(SRECLAIMABILE)

    def __find_line(self, line):
        size = 0
        with open(self.meminfo) as m:
            for l in m.readlines():
                if l.startswith(line):
                    size = toSize(*parseSize(l.split(':')[1].strip()), SIZE_BYTE)
                    break
        return size

    def to_dict(self):
        return {
                'total': self.total,
                'swap': self.swap,
                'meminfo': self.meminfo,
                'swapinfo': self.swapinfo,
                'used': self.used_mem,
                }

    @classmethod
    def from_dict(cls, dct):
        return cls(dct['total'], dct['swap'], dct['meminfo'], dct['swapinfo'])

    @classmethod
    def load_from_meminfo(cls, meminfo=MEMINFO, swaps=PROC_SWAPS):
        vals = read_meminfo(meminfo)
        return cls(vals[MEMTOTAL], vals[SWAPTOTAL])

def full_summary():
    return [ read_meminfo(), [s.to_dict() for s in read_swaps() ]]
