#!/usr/bin/python3
'''
Author : Zachary Harvey






'''

from . import disks
from . import mem
from . import cpuspp
def allinfo():
    return { 'storage': tuple([ d.to_dict() for d in disks.find_all_disks() ]),
             'mem': { 'swap' : [ s.to_dict() for s in mem.read_swaps() ],
                      'memory': mem.Memory.load_from_meminfo().to_dict() },
             'cpu': [ c.to_dict() for c in cpuspp.CPUInfo.load_all_sys_cpu() ],

            }


