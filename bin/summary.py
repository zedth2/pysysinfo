#!/usr/bin/python3

from json import dump
from sys import stdout

from pysysfs import allinfo

if __name__ == '__main__':
    dump(allinfo(), stdout, indent=2)
