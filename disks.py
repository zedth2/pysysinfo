#!/usr/bin/python3
'''
Author : Zachary Harvey






'''

from os.path import exists
from os import listdir
from glob import glob
from shutil import which
from subprocess import Popen, PIPE, TimeoutExpired
from pysysfs.consts import get_first_line
from pprint import pprint
from .fdiskhelps import raw_dump, parse_dump
from .sysexceptions import NoDisk, NoPartition
#from sysutils import helpers

SYSFS_BLOCK_PATH = '/sys/block/{disk}'
SYSFS_BLOCK = '/sys/block'
DEV_PATH = '/dev/{disk}'
MTAB = '/etc/mtab'
FSTAB = '/etc/fstab'
SFDISK = 'sfdisk'

UEVENT_MAJOR = 'MAJOR'
UEVENT_MINOR = 'MINOR'
UEVENT_DEVNAME = 'DEVNAME'
UEVENT_DEVTYPE = 'DEVTYPE'
UEVENT_PARTN = 'PARTN'


class Disk:
    def __init__(self, devname, loadparts=False):
        if isinstance(devname, dict):
            self.__from_dict(devname)
            return
        self.__sysblock = ''
        self.__devpath = ''
        self.__sys_parent = ''
        if not exists(DEV_PATH.format(disk=devname)):
            raise ValueError('Invalid disk {} : Device {} does not exist'.format(devname, DEV_PATH.format(disk=devname)))
        else:
            self.__devpath = DEV_PATH.format(disk=devname)
            if not exists(SYSFS_BLOCK_PATH.format(disk=devname)):
                parent = ''
                for c in devname:
                    if c.isalpha():
                        parent += c
                    else:
                        break
                if exists(SYSFS_BLOCK_PATH.format(disk=parent)+'/'+devname):
                    self.__sysblock = SYSFS_BLOCK_PATH.format(disk=parent)+'/'+devname
                    self.__sys_parent = SYSFS_BLOCK_PATH.format(disk=parent)
                else:
                    raise ValueError('Invalid disk {} : SYSFS {} or Child {} does not exist'.format(devname, SYSFS_BLOCK_PATH.format(disk=devname), SYSFS_BLOCK_PATH.format(disk=parent)+'/'+devname))
            else:
                self.__sysblock = SYSFS_BLOCK_PATH.format(disk=devname)
                self.__sys_parent = SYSFS_BLOCK_PATH.format(disk=devname)

        self.__devname = devname
        self.__major = 0
        self.__minor = 0
        self.__devtype = None
        self.__logicalBlockSize = 0
        self.__size = 0
        self.__partnum = None
        self.__mount = None
        self.__type = None
        self.__options = None
        self.__dump = None
        self.__fsck = None
        self.__start = None
        self.__sfdisk_raw = None
        self.parts = []
        self.holders = []
        self.read_mtab()
        self.load_from_sys(self.devname)
        self.load_holders()
        if loadparts:
            self.load_partitions()
        try:
            self.__sfdisk_raw = raw_dump(self.__devpath)
        except Exception:
            pass

    def load_minor(self, minor):
        p = Disk(self.devname + str(minor), self)
        self.parts.append(p)
        return p

    def read_mtab(self):
        ops = find_mtab_line(self.devpath)
        if ops == '':
            return False
        ops = ops.split()
        self.__mount = ops[1]
        self.__type = ops[2]
        self.__options = ops[3].split(',')
        self.__dump = ops[4]
        self.__fsck = ops[5]
        return True

    def load_sfdisk(self):
        if self.__sfdisk_raw == '':
            self.__sfdisk_raw = raw_dump(self.__devpath)

        self.__type = re['label']

    def load_from_sys(self, devname):
        if exists(self.__sysblock + '/uevent'):
            uevent = load_uevent(self.__sysblock + '/uevent')
            # self.__major = uevent[UEVENT_MAJOR]
            # self.__minor = uevent[UEVENT_MINOR]
            self.__devtype = uevent[UEVENT_DEVTYPE]
            # self.__partnum = uevent[UEVENT_PARTN] if UEVENT_PARTN in uevent else None

        if exists(self.__sysblock + '/partition'):
            self.__partnum = get_first_line(self.__sysblock + '/partition')
        self.__major, self.__minor = get_first_line(self.__sysblock + '/dev').split(':')

        self.__logicalBlockSize = int(get_first_line(self.__sys_parent + '/queue/logical_block_size'))

        self.__size = int(get_first_line(self.__sysblock + '/size')) * self.logicalBlockSize

    def load_sfdisk(self):
        pass

    def find_mount(self, mntpnt):
        part = None
        for p in self.parts:
            if p.mount is not None and p.mount == mntpnt:
                part = p
                break
        return part

    @property
    def type(self):
        '''
        The type the file system is formated in
        '''
        return self.__type
    @property
    def options(self):
        return self.__options
    @property
    def dump(self):
        return self.__dump
    @property
    def fsck(self):
        return self.__fsck
    @property
    def devname(self):
        return self.__devname

    @property
    def logicalBlockSize(self):
        '''
        Block Size I believe is always in bytes.
        '''
        return self.__logicalBlockSize

    @property
    def totalsize(self):
        '''
        Returns the disk size in bytes.
        '''
        return self.__size

    @property
    def devpath(self):
        return self.__devpath

    @property
    def sysfs_block(self):
        return self.__sysblock

    @property
    def major(self):
        return self.__major

    @property
    def minor(self):
        return self.__minor

    @property
    def devtype(self):
        return self.__devtype

    @property
    def partnum(self):
        return self.__partnum

    @property
    def mount(self):
        return self.__mount

    @property
    def start(self):
        return self.__start

    def sfdisk_raw_dump(self):
        return self.__sfdisk_raw

    def load_partitions(self):
        self.parts = []
        for d in glob(self.__sysblock+'/'+self.__devname+'[0-9]*'):
            disk = d.rsplit('/', 1)[-1]
            self.parts.append(Disk(disk, True))
        return self.parts

    def load_holders(self):
        self.holders = []
        for d in listdir(self.__sysblock + '/holders'):
            self.holders.append(Disk(d, True))

    def __from_dict(self, dct):
        self.__devname = dct['devname']
        self.__type = dct['type']
        self.__options = dct['options']
        self.__dump = dct['dump']
        self.__fsck = dct['fcsk']
        self.__logicalBlockSize = dct['logicalblocksize']
        self.__size = dct['totalsize']
        self.__devpath = dct['devpath']
        self.__sysblock = dct['syspath']
        self.__major = dct['major']
        self.__minor = dct['minor']
        self.__devtype = dct['devtype']
        self.__partnum = dct['partnum']
        self.__mount = dct['mount']
        for p in dct['partitions']:
            self.parts.append(Disk.from_dict(p))
        for h in dct['holders']:
            self.holders.append(Disk.from_dict(h))
        return self

    @classmethod
    def from_dict(cls, dct):
        return cls(dct)

    def to_dict(self):
        return {
                'devname' : self.devname,
                'totalsize': self.totalsize,
                'devpath': self.devpath,
                'partitions': [p.to_dict() for p in self.parts],
                'devtype': self.devtype,
                'major': self.major,
                'minor': self.minor,
                'syspath': self.__sysblock,
                'sysparent': self.__sys_parent,
                'holders': [h.to_dict() for h in self.holders],
                'mount': self.mount,
                'type': self.type,
                'options': self.options,
                'dump': self.dump,
                'fsck': self.fsck,
                'logicalblocksize': self.logicalBlockSize,
                'partnum': self.partnum,
                }

    def stop_pretty_str(self):
        re = ''
        for p in self.parts:
            re += p.pretty_str()
        return re

    def pretty_str(self):
        return '''Device : {}
Mount : {}
Size : {}
'''.format(self.devname, self.mount, self.totalsize)

def find_mtab_line(devpath):
    return _find_dev_line(devpath)

def find_fstab_line(devpath, fstab=FSTAB):
    return _find_dev_line(devpath, fstab)

def _find_dev_line(devpath, readfrom=MTAB):
    line = ''
    with open(readfrom) as tab:
        for l in tab.readlines():
            if l.split()[0] == devpath:
                line = l
                break
    return line

def load_all_mounts():
    disks = {}
    with open(MTAB) as tab:
        for l in tab.readlines():
            splits = l.split()
            if splits[0].startswith('/dev/sd') or splits[0].startswith('/dev/dm'): #Match on sd* because I don't feel like fixing matches for loop0 and sr0
                #helpers.debug('Loading device {}'.format(splits[0]))
                dev = splits[0][5:8]
                minor = splits[0][8]
                if not dev in disks:
                    disks[dev] = Disk(dev)
                p = disks[dev].load_minor(minor)
                p.read_mtab()
    return tuple(disks.values())

def find_mount(dirmnt):
    part = None
    for d in load_all_mounts():
        p = d.find_mount(dirmnt)
        if not p is None:
            part = p
            break
    return part

def find_all_disks():
    disks = {}
    for d in glob(SYSFS_BLOCK+'/sd*'):
        disk = d.rsplit('/', 1)[-1]
        disks[d.rsplit('/', 1)[-1]] = Disk(disk, True)
    return tuple(disks.values())

def find_disk(devname):
    try:
        d = glob(SYSFS_BLOCK + '/' + devname)[0].rsplit('/', 1)[-1]
        return Disk(d, True)
    except IndexError:
        raise NoDisk(str(devname) + ' was not found')

def find_disks(*disks):
    reDsks = []
    for d in disks:
        reDsks.append(find_disk(d))
    return reDsks

def load_uevent(uevent):
    f = open(uevent)
    lines = f.readlines()
    f.close()
    reVal = {}
    for l in lines:
        cnt = 0
        key = ''
        val = ''
        while cnt < len(l):
            if l[cnt] == '=':
                val = l[cnt+1:].strip()
                reVal[key] = val
                break
            key += l[cnt]
            cnt += 1
    return reVal
